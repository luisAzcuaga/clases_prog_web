<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="js/jquery-3.3.1.js">
	</script>
	<script>
		$(document).ready(function(){
			$("#txtCUP").change(function(){
				//consulta asincrona al servidor
				var txtCUP = $("#txtCUP").val();
				$.get("Server.php?txtCUP="+txtCUP,
					function(data, status){
						var dato = data.split(",");
						$("#txtDescripcion").val(dato[0]);
						$("#txtPrecio").val(dato[1]);
					});
			})
		});
	</script>
</head>
<body>
	<h1>Tienda Don Chuchito</h1>
	<form>
		<fieldset>
			<legend>Venta</legend>
			<label for="txtCUP"> Codigo</label>
			<input type="text" required="required" id="txtCUP"
			name="txtCUP" />
			<label for="txtDescripcion">Descripcion</label>
			<input type="text" id="txtDescripcion"
			name="txtDescripcion" />
			
			<label for="txtPrecio">Precio</label>
			<input type="text" id="txtPrecio"
			name="txtPrecio" />
			
		</fieldset>
		<button id="btnAgregar">Agregar</button>
	</form>
</body>
</html>
