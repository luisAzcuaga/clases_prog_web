<!DOCTYPE html>
<html>
<header>
	<link rel="stylesheet" type="text/css" href="index.css">
</header>
<head>
	<title>Agregar Productos</title>
	<script src="js/jquery-3.3.1.js"></script>

	<?php
	$servername = "localhost";
	$username = "luis";
	$password = "azcuaga";
	$dbname = "examen";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	//Get form values
	if ($_SERVER["REQUEST_METHOD"]=="POST"){
		$txtCodigo = htmlspecialchars($_REQUEST['txtCodigo']);
		$txtClaveParte = htmlspecialchars($_REQUEST['txtClaveParte']);


		$txtNombre = htmlspecialchars($_REQUEST['txtNombre']);
		$txtDescripcion = htmlspecialchars($_REQUEST['txtDescripcion']);
		$txtPrecio = htmlspecialchars($_REQUEST['txtPrecio']);

		$txtDescripcionParte = htmlspecialchars($_REQUEST['txtDescripcionParte']);
		$txtPrecioParte = htmlspecialchars($_REQUEST['txtPrecioParte']);
	}

	$sql = "INSERT INTO la_productos (la_codigo, la_nombre, la_descripcion, la_precio)
	VALUES ('$txtCodigo' , '$txtNombre', '$txtDescripcion', '$txtPrecio')";

	if ($conn->query($sql) === TRUE) {

		foreach ($tabla1 as $val){ 
			$sql = "INSERT INTO la_partes (la_clave, la_descripcion, la_precio)
			VALUES ('$txtClaveParte','$txtDescripcionParte','$txtPrecioParte')";
		}
		if ($conn->query($sql) === TRUE) {
			foreach ($tabla1 as $val){ 
				$sql = "INSERT INTO la_componentes (la_clave, la_codigo)
				VALUES ('$txtClaveParte','$txtCodigo')";
			}

			if ($conn->query($sql) === TRUE) {
				echo "<h2>¡DB Actualizada!</h2>";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}

	$conn->close();
	?>


	<script>
		$(document).ready(function(){
			$("#btnAgregar").click(function(){
				var txtClaveParte = $("#txtClaveParte").val();
				var txtDescripcionParte = $("#txtDescripcionParte").val();
				var txtPrecioParte = $("#txtPrecioParte").val();

				var renglon = "<tr><td>"
				+ txtClaveParte+"</td><td>"
				+ txtDescripcionParte +"</td><td>"
				+ txtPrecioParte+"</td></tr>";

				$("#tabla1").append(renglon);
				var suma = $("#txtPrecio");
				$("#txtPrecio").val( (suma + txtPrecioParte ));
			});
		});
	</script>
</head>
<body>
	<div id="navBar" style="position: fixed;">
		<div>
			<h1 id="titulo_header">EMPRESA UNIVERSAL</h1>
		</div>
		<div>
			<nav>
				<ul id="pagePicker">
					<li> <a href="inicio.php">Inicio</a> </li>		
					<li id="currentPage"> <a href="agregarProductos.php">Añadir Productos</a></li>
					<li> <a href="verProductos.php">Ver Productos</a> </li>
					<li> <a href="verPartes.php">Ver Partes</a> </li>
					<li> <a href="index.php">Cerrar Sesión</a> </li>
				</ul>
			</nav>
		</div>
	</div>
	<div id="content">
		<form form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
			<fieldset>
				<fieldset>
					<h2>Producto Nuevo</h2>
					<label for ="txtCodigo">Código</label>
					<input type="number" name="txtCodigo" id="txtCodigo">
					<br />
					<label for ="txtNombre">Nombre</label>
					<input type="text" name="txtNombre" id="txtNombre">
					<br />
					<label for ="txtDescripcion">Descripcion</label>
					<input type="text" name="txtDescripcion" id="txtDescripcion">
					<br />
					<label for ="txtPrecio">Precio</label>
					<input type="text" name="txtPrecio" id="txtPrecio">
				</fieldset>
				<br />

				<fieldset>
					<h2>Partes de Producto nuevo</h2>
					<label for ="txtClaveParte">Clave</label>
					<input type="number" name="txtClaveParte" id="txtClaveParte" onchange="mostrarParte(this.value)">
					<br />
					<label for ="txtDescripcionParte">Descripcion</label>
					<input type="text" name="txtDescripcionParte" id="txtDescripcionParte">
					<br />
					<label for ="txtPrecioParte">Precio</label>
					<input type="text" name="txtPrecioParte" id="txtPrecioParte">
					<br/>
					<br/>
				</fieldset>
			</fieldset>

			<input type="submit" value="Terminar Producto"/>
		</form>

		<fieldset>
			<table id="tabla1" border="5">
				<tr><th>Parte</th> <th>Descripción</th><th>Precio</th> </tr>
			</table>
			<br/>
			<button id="btnAgregar" class="TEST">Agregar Parte</button>
		</fieldset>

		<script>
			function mostrarParte(str) {
				if (str == "") {
					document.getElementById("txtClaveParte").innerHTML = "";
					return;
				} else {  
					if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function() {
        	if (this.readyState == 4 && this.status == 200) {
        		var result = this.responseText.split(",");
        		//document.getElementById("txtClaveParte").value = result[0];
        		if (result[1] != undefined) {
        			document.getElementById("txtDescripcionParte").value = result[1];
        		} else {
        			document.getElementById("txtDescripcionParte").value = "";
        		}
        		if (result[2] != undefined) {
        			document.getElementById("txtPrecioParte").value = result[2];
        		} else {
        			document.getElementById("txtPrecioParte").value = "";
        		}
        	}
        };
        xmlhttp.open("GET","autocompletar.php?query="+str,true);
        xmlhttp.send();
    }
}
</script>


</div>
</body>
</html>