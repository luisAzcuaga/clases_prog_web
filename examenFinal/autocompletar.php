<?php

if (isset($_GET['query']) && is_numeric($_GET['query'])) {
    $query = (int) $_GET['query'];
} else {
    $query = 1;
}

$mysqli = new mysqli('localhost', 'luis', 'azcuaga', 'examen');

if ($mysqli->connect_errno) {
 
    echo "Lo sentimos, este sitio web está experimentando problemas.";

    echo "Error: Fallo al conectarse a MySQL debido a: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    
    exit;
}

$sql = "SELECT la_clave, la_descripcion, la_precio FROM la_partes WHERE la_clave = $query";
if (!$resultado = $mysqli->query($sql)) {

    echo "Lo sentimos, este sitio web está experimentando problemas.";

    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

if ($resultado->num_rows === 0) {
    
    echo "Lo sentimos. No se pudo encontrar una coincidencia para el ID $query. Inténtelo de nuevo.";
    exit;
}

$result_f = $resultado->fetch_assoc();
echo $result_f['la_clave'] . "," . $result_f['la_descripcion'] . "," . $result_f['la_precio'];

$resultado->free();
$mysqli->close();
?>